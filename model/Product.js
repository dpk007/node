const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productSchema = new Schema({
  name: { type: String, require: true },
  price: { type: String, require: true },
  quantity: { type: String, require: true },
  discription: { type: String },
  createdAt: { type: Date, default: Date.now() },
  updatedAt: { type: Date ,default:null}
});

module.exports = mongoose.model("product", productSchema);
