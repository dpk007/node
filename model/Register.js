const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const registerSchema = new Schema({
  name: { type: String, required: true, createIndex: { unique: true } },
  email: { type: String, required: true, createIndex: { unique: true } },
  city: { type: String,default:'' },
  state: { type: String ,default:''},
  pincode: { type: String ,default:''},
  mobile: { type: Number},
  profilePic: { type: String ,default:''},
  password: { type: String, required: true },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Users = mongoose.model("users", registerSchema);
