const mongoose = require('mongoose');
const server = "127.0.0.1:27017";
const databaseName = "react";
const dbUrl = `mongodb://${server}/${databaseName}`;
class Database {

  constructor() {
    this._connect();
  }

   _connect() {

    try {
      mongoose.connect(dbUrl, {useNewUrlParser: true})
      .then( () =>{
        console.log(`connected to database successfully!`);
      })
      .catch(err => {
        console.log(err);
      })
      
     
    } catch (err) {
      console.log(err);
     
    }
  }

}
module.exports = new Database();
