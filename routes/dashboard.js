const express = require("express");
const route = express.Router();
const Users = require("../model/Register");
const passport = require("passport");

// @type    GET
//@route    /dashboard
// @desc    route dashboard data
// @access  PRIVATE
route.post(
    "/",
    passport.authenticate("jwt", { session: false }),
    (req, res) => {
        Users.find()
        .then(users => {
          if (!users) {
            return res.status(404).json({ usersNotFound: "No User Found" });
          }
          res.json(users);
        })
        .catch(err => console.log("got some error in profile " + err));
    }
  );

  module.exports = route;