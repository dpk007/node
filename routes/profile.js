const express = require("express");
const router = express.Router();
const passport = require("passport");
const Profile = require("../model/Register");

/*
@TYPE POST
@ACCESS PRIVATE
@Update profile info
*/
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const email = req.body.email;
    const name = req.body.name;
    const city = req.body.city;
    const state = req.body.state;
    const pincode = req.body.pincode;
    const mobile = req.body.mobile;
    const data = {
      email: email,
      name: name,
      city: city,
      state: state,
      pincode: pincode,
      mobile: mobile
    };
    Profile.findOne({ email: email })
      .then(user => {
        if (user && user.email !== email) {
          return res.state(400).json({ message: "Email already exist" });
        } else {
          Profile.updateOne({ email: email }, { $set: data })
            .then(res => res.json({ message: "Successfully updated" }))
            .catch(err => {
              res.json(err);
            });
        }
      })
      .catch(err => {
        res.status(400).json(err);
      });
  }
);

/*
@TYPE GET
@ACCESS PRIVATE
@get user profile info
*/
router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const email = req.query.email;
    Profile.findOne({ email: email })
      .then(user => {
        res.json(user);
      })
      .catch(err => {
        res.json(err);
      });
  }
);

module.exports = router;
