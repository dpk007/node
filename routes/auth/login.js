const express = require("express");
const route = express.Router();
const bcrypt = require("bcrypt");
const Users = require("../../model/Register");
const key = require("../../config");
const jsonwt = require("jsonwebtoken");

route.post("/", (req, res) => {
  const password = req.body.password;
  Users.findOne({ email: req.body.email }).then(user => {
    if (!user) {
      return res.status(400).send({ errMsg: "Email does not exist" });
    }
    
    bcrypt.compare(password, user.password).then(isCorrect => {
      if (!isCorrect) {
        return res.status(400).send({ errMsg: "Password does not match" });
      }
      let payload = {
        id: user.id,
        email: user.email,
        name: user.name
      };

      jsonwt.sign(payload, key.secret, { expiresIn: 3600 }, (err, token) => {
        res.json({
          success: true,
          token: "Bearer " + token,
          info:user
        });
      });
    });
  });
});

module.exports = route;
