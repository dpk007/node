var express = require("express");
var router = express.Router();
const bycrypt = require("bcrypt");
var Users = require("../../model/Register");

router.post("/", function(req, res) {
  // console.log(req);

  var data = { name: req.body.Name, email: req.body.Email, password: req.body.Password };
  Users.findOne({ email: req.body.Email })
    .then(user => {
      if (user) {
        res.status(400).json({ errMsg: "Email already exist" });
      } else {
        users = new Users(data);
        bycrypt.genSalt(10, (err, salt) => {
          bycrypt.hash(users.password, salt, (err, hash) => {
            if (err) throw err;
            users.password = hash;
            users
              .save()
              .then(person =>
                res
                  .status(200)
                  .json({ success: 1, message: "Successfully registered." })
              )
              .catch(err => console.log(err));
          });
        });
      }
    })
    .catch(err => {});
});

module.exports = router;
