const express = require("express");
const router = express.Router();
const Product = require("../model/Product");
const passport = require("passport");

/*
add product
 */
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const product_name = req.body.product;
    const price = req.body.price;
    const quantity = req.body.quantity;

    let data = { name: product_name, price: price, quantity: quantity };

    let product = new Product(data);
    product
      .save()
      .then(response => {
        res.json({ message: "Product Added successfuly!" });
      })
      .catch(err => {
        res.status(400).json({ message: "Not able to add product" });
      });
  }
);


/*
get single product information
*/
router.post(
  "/productDetails",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const productId = req.body.id;
    Product.findOne({'_id':productId})
      .then(response => {
        res.json(response);
      })
      .catch(err => {
        res.status(400).json({ message: "Not able to get product" });
      });
  }
);


/*
delete product
*/
router.post(
  "/productDelete",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const productId = req.body.id;
     console.log('product ids',productId);
    Product.remove({'_id':{$in:productId}})
      .then(response => {
        res.json(response);
      })
      .catch(err => {
        res.status(400).json({ message: "Not able to delete product" });
      });
  }
);

/*
edit  product
*/
router.post(
  "/editProduct",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const productId = req.body.id;
    const name = req.body.name;
    const price = req.body.price;
    const quantity = req.body.quantity;
    data = {name,price,quantity};
    console.log(data,productId);
    Product.findOne({'_id':productId})
      .then(product => {
            if(!product){
               return res.state(400).json();
            }
            Product.updateOne({_id:productId},{$set:data})
               .then(response => {
                     res.json({'message':'Updated successfully'});
               })
               .catch(err =>{
                   res.json(err);
               })
      })
      .catch(err => {
        res.status(400).json({ message: "Not able to delete product" });
      });
  }
);


/*
get all product information
 */
router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Product.find({})
      .select({"name":"1", "price":"1", "quantity":"1"})
      .then(product => {
        res.json(product);
      })
      .catch(err => {
        res.json(err);
      });
  }
);
module.exports = router;
