const express = require("express");
const app = express();
const cors = require("cors");
const myParser = require("body-parser");
const logger = require("morgan");
const passport = require('passport');


const connection = require("./model/connection");
app.use(logger('dev'));
app.use(myParser.json());
app.use(cors());
app.use(express.static('public'));


//Passport middleware
app.use(passport.initialize());

//Config for JWT strategy
require("./strategies/jsonwtStrategy")(passport);
/*
  @type POST
  Registration Route
  @access Public
  */
const regRoute = require("./routes/auth/register");
const loginRoute = require("./routes/auth/login");
const dashBoardRoute = require("./routes/dashboard");
const profileRoute = require("./routes/profile");
const productRoute = require("./routes/product");
app.use("/register", regRoute);
app.use("/login", loginRoute);
app.use("/dashboard", dashBoardRoute);
app.use("/profile", profileRoute);
app.use("/product", productRoute);


const port = process.env.PORT || 9000;

app.listen(port, () => {
  console.log(`app is running on port ${port}`);

});
